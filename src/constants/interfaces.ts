export interface NetworkStats {
    timestamp: number
}

export interface PoolStats {

}

export interface PoolConfigs {
    
}

export interface Block {

}

export interface Payment {
    
}

export interface AuthResponse {
    msg: string
}

export interface PageParams {
    slug: string,
    title: string,
    icon: string
}

export interface Ports {
    
}